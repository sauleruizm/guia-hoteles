$(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({interval:2000});

    $("#exampleModal").on("show.bs.modal",function(e){
      console.log("El modal se está mostrando");
      $("#contactobtn").removeClass("btn-outline-success");
      $("#contactobtn").addClass("btn-primary");
      $("#contactobtn").prop("disabled",true);
    });
    $("#exampleModal").on("shown.bs.modal",function(e){
      console.log("El modal se mostró");
    });

    $("#exampleModal").on("hide.bs.modal",function(e){
      console.log("El modal se oculta");
    });
    $("#exampleModal").on("hidden.bs.modal",function(e){
      console.log("El modal se ocultó");
    });
  });